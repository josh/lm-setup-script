sudo apt-get install -y 0ad audacious audacity backintime-gnome build-essential chromium-browser cryptsetup curl flac fslint
geoclue-2.0 redshift git git-gui gitk gnome-search-tool gnupg2 gpa gparted hddtemp iotop jpegoptim lame lm-sensors nmap oathtool openssh-server opus-tools p7zip-full playonlinux psensor soundconverter steam photorec testdisk unoconv vinagre opus-tools vorbis-tools wine simplescreenrecorder

git config --global user.email "josh@joshmudge.com"
git config --global user.name "Josh Mudge"

wget https://atom.io/download/deb
sudo dpkg -i deb

rm deb

wget https://zoom.us/client/latest/zoom_amd64.deb
sudo dpkg -i zoom_amd64.deb

rm zoom_amd64.deb

curl -fsL bit.ly/node-installer | bash

wget https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh

sudo chmod +x Anaconda3-2018.12-Linux-x86_64.sh

./Anaconda3-2018.12-Linux-x86_64.sh

rm Anaconda3-2018.12-Linux-x86_64.sh
